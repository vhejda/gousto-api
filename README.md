Gousto Recipes REST api
===
This application is built on Symfony3 framework using FOSRestBundle for easy integration of RESTful principles.

Controller is independent of a response format. Although only JSON is enabled at this moment (via configuration file), 
adding support for other formats such as XML or HTML (with a template) for different API consumers would be very easy.

## Installation and running
1. Unzip the archive
1. In the project root call `composer install`
1. In the project root run `php bin/console server:run` for a simple PHP server
1. API is available on `localhost:8000`

## Available routes
1. `GET /api/recipes.json` - Fetch recipes
   * Pagination with query string parameters "offset" and "limit":
     * `/api/recipes.json?offset=0&limit=2`
   * Filter by parameter "cuisine":
     * `/api/recipes.json?cuisine=mexican`
1. `POST /api/recipes.json` - Store a new recipe
   * Accepts recipe data in JSON string in a request body 
1. `GET /api/recipes/{id}.json` - Fetch a recipe by its ID
1. `PUT /api/recipes/{id}.json` - Puts a recipe to this URI
   * Accepts recipe data in JSON string in a request body
1. `GET /api/recipes/{id}/ratings.json` - Get ratings for this recipe
   * Pagination with query string parameters "offset" and "limit":
     * `/api/recipes/{id}/ratings.json?offset=0&limit=2`
1. `POST /api/recipes/{id}/ratings.json` - Post a rating for this recipe
   * Accepts rating in JSON string in a request body - e.g.: `{"rating":4}` 
   
## Notes
Currently there is no validation set up for recipes (no required fields). It will only store posted columns that match 
with headers in the recipes CSV, leave empty those not present and ignore all the rest.

