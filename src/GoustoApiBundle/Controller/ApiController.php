<?php

namespace GoustoApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcherInterface;
use GoustoApiBundle\Entity\Recipe;
use GoustoApiBundle\Entity\RecipeRating;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiController extends FOSRestController
{

    /**
     * Fetch all recipes / recipes for a specific cuisine - GET /api/recipes.{_format}
     *
     * @QueryParam(name="cuisine", description="recipe_cuisine")
     * @QueryParam(name="offset", requirements="\d+", default="0")
     * @QueryParam(name="limit", requirements="\d+")
     *
     * @param ParamFetcherInterface $paramFetcher
     * @return Response
     */
    public function getRecipesAction(ParamFetcherInterface $paramFetcher)
    {
        // Filtration a pagination:
        $cuisine = $paramFetcher->get('cuisine');
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        if ($limit === "") {
            $limit = null;          // unlimited number of recipes
        }

        // Filter by cuisine?
        if ($cuisine) {
            $recipes = $this->get("gousto.recipe.repository")->fetchByCuisine($cuisine, $offset, $limit);
        }else{
            $recipes = $this->get("gousto.recipe.repository")->fetchAll($offset, $limit);
        }
        $view = $this->view($recipes, 200);

        return $this->handleView($view);
    }

    /**
     * Post a new recipe - POST /api/recipes.{_format}
     *
     * @param Request $request
     * @return Response
     */
    public function postRecipesAction(Request $request)
    {
        // Decode data:
        if (empty($request->getContent())) {
            throw new BadRequestHttpException("Missing request body");
        }
        $data = $this->get("serializer")
            ->decode($request->getContent(), $request->attributes->get("_format"));

        /** @var Recipe $recipe */
        $recipe = $this->get("gousto.recipe.repository")->createFromData($data);

        // Persist recipe to storage:
        $this->get("gousto.recipe.repository")->insert($recipe);

        // Redirect to the newly created resource:
        $view = $this->routeRedirectView(
            "get_recipe",
            array("id" => $recipe->getId(), "_format" => $request->attributes->get("_format"))
        );

        return $this->handleView($view);
    }

    /**
     * Fetch a recipe by id - GET /api/recipes/{id}.{_format}
     *
     * @param int $id
     * @return Response
     */
    public function getRecipeAction($id)
    {
        $recipe = $this->get("gousto.recipe.repository")->fetchById($id);
        if (!$recipe) {
            throw new NotFoundHttpException("Recipe with id: $id not found");
        }

        $view = $this->view($recipe, 200);

        return $this->handleView($view);
    }

    /**
     * Insert/update recipe at this id - PUT /api/recipes/{id}.{_format}
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function putRecipeAction(Request $request, $id)
    {
        // Decode data:
        if (empty($request->getContent())) {
            throw new BadRequestHttpException("Missing request body");
        }
        $data = $this->get("serializer")
            ->decode($request->getContent(), $request->attributes->get("_format"));

        /** @var Recipe $recipe */
        $recipe = $this->get("gousto.recipe.repository")->fetchById($id);
        if (!$recipe) {
            // create Recipe from Request data with specified ID:
            $recipe = $this->get("gousto.recipe.repository")->createFromData($data, $id);

            // Persist recipe to storage:
            $this->get("gousto.recipe.repository")->insert($recipe);
            $view = $this->view($recipe, 201);
        }else{
            // Update recipe in storage:
            $recipe = $this->get("gousto.recipe.repository")->updateWithData($recipe, $data);
            $view = $this->view($recipe, 200);
        }

        return $this->handleView($view);
    }

    /**
     * Fetch ratings for specific Recipe - GET /api/recipes/{recipeId}/ratings.{_format}
     *
     * @QueryParam(name="offset", requirements="\d+", default="0")
     * @QueryParam(name="limit", requirements="\d+")
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param int $recipeId
     * @return Response
     */
    public function getRecipeRatingsAction(ParamFetcherInterface $paramFetcher, $recipeId)
    {
        // Pagination:
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        if ($limit === "") {
            $limit = null;          // unlimited number of ratings
        }

        // Fetch ratings for Recipe with $recipeId:
        $ratings = $this->get('gousto.recipe_rating.repository')
            ->fetchByRecipe($recipeId, $offset, $limit);
        $view = $this->view($ratings, 200);

        return $this->handleView($view);
    }

    /**
     * Post a new recipe rating - POST /api/recipes/{recipeId}/ratings.{_format}
     *
     * @param Request $request
     * @param int $recipeId
     * @return Response
     */
    public function postRecipeRatingsAction(Request $request, $recipeId)
    {
        // Decode data:
        if (empty($request->getContent())) {
            throw new BadRequestHttpException("Missing request body");
        }
        $data = $this->get("serializer")
            ->decode($request->getContent(), $request->attributes->get("_format"));

        if (!isset($data["rating"]) or intval($data["rating"]) > 5 or intval($data["rating"]) < 1) {
            throw new BadRequestHttpException("Please specify 'rating' between 1 and 5.");
        }

        /** @var RecipeRating $rating */
        $rating = $this->get("gousto.recipe_rating.repository")->create($recipeId, $data["rating"]);

        // Persist recipe to storage:
        $this->get("gousto.recipe_rating.repository")->insert($rating);

        // Redirect to the newly created resource:
        $view = $this->routeRedirectView(
            "get_recipe_ratings",
            array("recipeId" => $recipeId, "_format" => $request->attributes->get("_format"))
        );

        return $this->handleView($view);
    }

}
