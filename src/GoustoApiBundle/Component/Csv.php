<?php

namespace GoustoApiBundle\Component;

/**
 * Class Csv
 * - Enables safe reading from / writing to a csv file
 *
 * @author Vojtěch Hejda
 */
class Csv
{
    const TEMP_FILE = "temp.csv";

    /**
     * @var resource
     */
    protected $handle;

    /**
     * @var string path to CSV file
     */
    protected $file_name;

    /**
     * @param string $file path to CSV file
     * @throws \Exception
     */
    public function __construct($file)
    {
        $this->file_name = $file;
        $this->open($file);
    }

    /**
     * @param string $file path to CSV file
     * @throws \Exception
     */
    private function open($file)
    {
        if (!file_exists($file)) {
            throw new \Exception("Attempted to open non-existing file ");
        }
        $this->handle = fopen($file, "a+");     // "writes" will always be appended
        rewind($this->handle);                  // read from the beginning
    }

    public function rewind()
    {
        rewind($this->handle);                  // read from the beginning
    }

    /**
     * @param array $columns
     * @return bool success
     */
    public function addLine($columns)
    {
        if (!flock($this->handle, LOCK_EX)) {
            return false;                       // could not get an exclusive lock on the file
        }
        fputcsv($this->handle, $columns);
        fflush($this->handle);
        flock($this->handle, LOCK_UN);
        return true;
    }

    /**
     * @return array columns
     */
    public function getNextLine()
    {
        return fgetcsv($this->handle);
    }

    /**
     * Seek a line with a specific value in a column and replace this line with new content
     *
     * @param int $column_number
     * @param mixed $value
     * @param array $new_columns
     * @return bool
     */
    public function replaceLineBasedOnColumnValue($column_number, $value, $new_columns)
    {
        // uses temporary file for this operation
        $temp = fopen(self::TEMP_FILE, 'w');

        // read all lines:
        $this->rewind();
        while ($line = fgetcsv($this->handle)) {
            if ($line[$column_number] == $value) {
                //Replace this line:
                $line = $new_columns;
            }
            fputcsv($temp, $line);
        }
        $this->close();
        fclose($temp);

        // delete old file and replace it with temp
        unlink($this->file_name);
        $success = rename(self::TEMP_FILE, $this->file_name);
        $this->open($this->file_name);

        return $success;
    }

    /**
     * @return bool success
     */
    public function close()
    {
        return fclose($this->handle);
    }

}
