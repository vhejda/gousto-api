<?php

namespace GoustoApiBundle\Entity;

class Recipe
{

    const DATETIME_FORMAT = "d/m/Y H:i:s";

    /** @var int $id */
    protected $id;

    /** @var \DateTime $createdAt */
    protected $createdAt;

    /** @var \DateTime $updatedAt */
    protected $updatedAt;

    /** @var string $boxType */
    protected $boxType;

    /** @var string $title */
    protected $title;

    /** @var string $slug */
    protected $slug;

    /** @var string $shortTitle */
    protected $shortTitle;

    /** @var string $marketingDescription */
    protected $marketingDescription;

    /** @var int $caloriesKcal */
    protected $caloriesKcal;

    /** @var int $proteinGrams */
    protected $proteinGrams;

    /** @var int $fatGrams */
    protected $fatGrams;

    /** @var int $carbsGrams */
    protected $carbsGrams;

    /** @var string $bulletpoint1 */
    protected $bulletpoint1;

    /** @var string $bulletpoint2 */
    protected $bulletpoint2;

    /** @var string $bulletpoint3 */
    protected $bulletpoint3;

    /** @var string $recipeDietTypeId*/
    protected $recipeDietTypeId;

    /** @var string $season */
    protected $season;

    /** @var string $base */
    protected $base;

    /** @var string $proteinSource */
    protected $proteinSource;

    /** @var int $preparationTimeMinutes */
    protected $preparationTimeMinutes;

    /** @var int $shelfLifeDays */
    protected $shelfLifeDays;

    /** @var string $equipmentNeeded */
    protected $equipmentNeeded;

    /** @var string $originCountry */
    protected $originCountry;

    /** @var string $recipeCuisine */
    protected $recipeCuisine;

    /** @var string $inYourBox */
    protected $inYourBox;

    /** @var int $goustoReference */
    protected $goustoReference;

    /**
     * Recipe constructor.
     * @param int $id
     */
    public function __construct($id)
    {
        $this->id = (int)$id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string Date in self::DATETIME_FORMAT format
     */
    public function getCreatedAt()
    {
        return $this->createdAt ? $this->createdAt->format(self::DATETIME_FORMAT) : "";
    }

    /**
     * @param string|\DateTime $createdAt
     * @return Recipe
     */
    public function setCreatedAt($createdAt)
    {
        if ($createdAt instanceof \DateTime) {
            $this->createdAt = $createdAt;
        } elseif (is_string($createdAt)){
            $this->createdAt = \DateTime::createFromFormat(self::DATETIME_FORMAT, $createdAt);
        }
        return $this;
    }

    /**
     * @return string Date in self::DATETIME_FORMAT format
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt ? $this->updatedAt->format(self::DATETIME_FORMAT) : "";
    }

    /**
     * @param string|\DateTime $updatedAt
     * @return Recipe
     */
    public function setUpdatedAt($updatedAt)
    {
        if ($updatedAt instanceof \DateTime) {
            $this->updatedAt = $updatedAt;
        } elseif (is_string($updatedAt)){
            $this->updatedAt = \DateTime::createFromFormat(self::DATETIME_FORMAT, $updatedAt);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getBoxType()
    {
        return $this->boxType;
    }

    /**
     * @param string $boxType
     * @return Recipe
     */
    public function setBoxType($boxType)
    {
        $this->boxType = $boxType;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Recipe
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Recipe
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortTitle()
    {
        return $this->shortTitle;
    }

    /**
     * @param string $shortTitle
     * @return Recipe
     */
    public function setShortTitle($shortTitle)
    {
        $this->shortTitle = $shortTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getMarketingDescription()
    {
        return $this->marketingDescription;
    }

    /**
     * @param string $marketingDescription
     * @return Recipe
     */
    public function setMarketingDescription($marketingDescription)
    {
        $this->marketingDescription = $marketingDescription;
        return $this;
    }

    /**
     * @return int
     */
    public function getCaloriesKcal()
    {
        return $this->caloriesKcal;
    }

    /**
     * @param int $caloriesKcal
     * @return Recipe
     */
    public function setCaloriesKcal($caloriesKcal)
    {
        $this->caloriesKcal = (int)$caloriesKcal;
        return $this;
    }

    /**
     * @return int
     */
    public function getProteinGrams()
    {
        return $this->proteinGrams;
    }

    /**
     * @param int $proteinGrams
     * @return Recipe
     */
    public function setProteinGrams($proteinGrams)
    {
        $this->proteinGrams = (int)$proteinGrams;
        return $this;
    }

    /**
     * @return int
     */
    public function getFatGrams()
    {
        return $this->fatGrams;
    }

    /**
     * @param int $fatGrams
     * @return Recipe
     */
    public function setFatGrams($fatGrams)
    {
        $this->fatGrams = (int)$fatGrams;
        return $this;
    }

    /**
     * @return int
     */
    public function getCarbsGrams()
    {
        return $this->carbsGrams;
    }

    /**
     * @param int $carbsGrams
     * @return Recipe
     */
    public function setCarbsGrams($carbsGrams)
    {
        $this->carbsGrams = (int)$carbsGrams;
        return $this;
    }

    /**
     * @return string
     */
    public function getBulletpoint1()
    {
        return $this->bulletpoint1;
    }

    /**
     * @param string $bulletpoint1
     * @return Recipe
     */
    public function setBulletpoint1($bulletpoint1)
    {
        $this->bulletpoint1 = $bulletpoint1;
        return $this;
    }

    /**
     * @return string
     */
    public function getBulletpoint2()
    {
        return $this->bulletpoint2;
    }

    /**
     * @param string $bulletpoint2
     * @return Recipe
     */
    public function setBulletpoint2($bulletpoint2)
    {
        $this->bulletpoint2 = $bulletpoint2;
        return $this;
    }

    /**
     * @return string
     */
    public function getBulletpoint3()
    {
        return $this->bulletpoint3;
    }

    /**
     * @param string $bulletpoint3
     * @return Recipe
     */
    public function setBulletpoint3($bulletpoint3)
    {
        $this->bulletpoint3 = $bulletpoint3;
        return $this;
    }

    /**
     * @return string
     */
    public function getRecipeDietTypeId()
    {
        return $this->recipeDietTypeId;
    }

    /**
     * @param string $recipeDietTypeId
     * @return Recipe
     */
    public function setRecipeDietTypeId($recipeDietTypeId)
    {
        $this->recipeDietTypeId = $recipeDietTypeId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * @param string $season
     * @return Recipe
     */
    public function setSeason($season)
    {
        $this->season = $season;
        return $this;
    }

    /**
     * @return string
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @param string $base
     * @return Recipe
     */
    public function setBase($base)
    {
        $this->base = $base;
        return $this;
    }

    /**
     * @return string
     */
    public function getProteinSource()
    {
        return $this->proteinSource;
    }

    /**
     * @param string $proteinSource
     * @return Recipe
     */
    public function setProteinSource($proteinSource)
    {
        $this->proteinSource = $proteinSource;
        return $this;
    }

    /**
     * @return int
     */
    public function getPreparationTimeMinutes()
    {
        return $this->preparationTimeMinutes;
    }

    /**
     * @param int $preparationTimeMinutes
     * @return Recipe
     */
    public function setPreparationTimeMinutes($preparationTimeMinutes)
    {
        $this->preparationTimeMinutes = (int)$preparationTimeMinutes;
        return $this;
    }

    /**
     * @return int
     */
    public function getShelfLifeDays()
    {
        return $this->shelfLifeDays;
    }

    /**
     * @param int $shelfLifeDays
     * @return Recipe
     */
    public function setShelfLifeDays($shelfLifeDays)
    {
        $this->shelfLifeDays = (int)$shelfLifeDays;
        return $this;
    }

    /**
     * @return string
     */
    public function getEquipmentNeeded()
    {
        return $this->equipmentNeeded;
    }

    /**
     * @param string $equipmentNeeded
     * @return Recipe
     */
    public function setEquipmentNeeded($equipmentNeeded)
    {
        $this->equipmentNeeded = $equipmentNeeded;
        return $this;
    }

    /**
     * @return string
     */
    public function getOriginCountry()
    {
        return $this->originCountry;
    }

    /**
     * @param string $originCountry
     * @return Recipe
     */
    public function setOriginCountry($originCountry)
    {
        $this->originCountry = $originCountry;
        return $this;
    }

    /**
     * @return string
     */
    public function getRecipeCuisine()
    {
        return $this->recipeCuisine;
    }

    /**
     * @param string $recipeCuisine
     * @return Recipe
     */
    public function setRecipeCuisine($recipeCuisine)
    {
        $this->recipeCuisine = $recipeCuisine;
        return $this;
    }

    /**
     * @return string
     */
    public function getInYourBox()
    {
        return $this->inYourBox;
    }

    /**
     * @param string $inYourBox
     * @return Recipe
     */
    public function setInYourBox($inYourBox)
    {
        $this->inYourBox = $inYourBox;
        return $this;
    }

    /**
     * @return int
     */
    public function getGoustoReference()
    {
        return $this->goustoReference;
    }

    /**
     * @param int $goustoReference
     * @return Recipe
     */
    public function setGoustoReference($goustoReference)
    {
        $this->goustoReference = (int)$goustoReference;
        return $this;
    }

}