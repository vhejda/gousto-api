<?php

namespace GoustoApiBundle\Entity;

use GoustoApiBundle\Component\Csv;
use Symfony\Component\Serializer\Serializer;

class RecipeRepository
{
    const DATA_FILE = "data/recipe.csv";
    const DATA_CLASS = 'GoustoApiBundle\Entity\Recipe';

    /**
     * @var Csv
     */
    protected $storage;

    /**
     * @var array|Recipe[]
     */
    protected $recipes;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @var string path to the application root
     */
    protected $app_root;

    /**
     * @param Serializer $serializer
     * @param string $kernel_root
     * @param Csv $storage
     */
    public function __construct($serializer, $kernel_root, Csv $storage = null)
    {
        $this->serializer = $serializer;
        $this->app_root = $kernel_root . "/../";

        // if not open, open CSV containing the recipe data:
        if ($storage === null) {
            $this->storage = new Csv($this->app_root . self::DATA_FILE);
        }else{
            $this->storage = $storage;
        }

        // Load recipes:
        $this->loadRecipesFromCsv();
    }

    /**
     * @param Csv $storage
     */
    public function setStorage(Csv $storage)
    {
        $this->storage = $storage;
        // Load recipes:
        $this->loadRecipesFromCsv();
    }

    /**
     * @return Csv
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * Loading of the recipes
     */
    protected function loadRecipesFromCsv()
    {
        $this->storage->rewind();
        // Get column headers from the first line of the CSV:
        $column_names = $this->storage->getNextLine();

        // Instantiate array of Recipe objects by denormalizing assoc. array returned from CSV line
        // Denormalization uses "Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer"
        // with "CamelCaseToSnakeCaseNameConverter"
        $this->recipes = array();
        while ($values = $this->storage->getNextLine()) {
            $this->recipes[] = $this->serializer->denormalize(
                array_combine($column_names, $values),
                self::DATA_CLASS
            );
        }
    }

    /**
     * Assuming recipes are always ordered by id in storage, and id is unchangeable,
     * take the last element in recipes array add increment it by 1
     *
     * @return int
     */
    protected function getNewRecipeId()
    {
        return end($this->recipes)->getId() + 1;
    }

    /**
     * Simple slug function
     * - only works on English characters and doesn't ensure uniqueness
     *
     * @param $string
     * @return mixed
     */
    protected function simpleSlug($string)
    {
        return preg_replace('/[^a-z0-9]/', '-', strtolower(trim(strip_tags($string))));
    }

    // --------------------------------------------
    //  Repository methods:
    // --------------------------------------------

    /**
     * @param array $data
     * @param int|null $id
     * @return Recipe
     */
    public function createFromData($data, $id = null)
    {
        $data['id'] = $id ? $id : $this->getNewRecipeId();
        $data['slug'] = $this->simpleSlug($data['title']);
        $data['created_at'] = new \DateTime();
        $data['updated_at'] = new \DateTime();

        return $this->serializer->denormalize(
            $data,
            self::DATA_CLASS
        );
    }

    /**
     * Fetches all recipes
     *
     * @param int $offset
     * @param int $limit
     * @return array|Recipe[]
     */
    public function fetchAll($offset = 0, $limit = null)
    {
        return array_slice($this->recipes, $offset, $limit);
    }

    /**
     * Fetches recipes for specific cuisine
     *
     * @param string $cuisine
     * @param int $offset
     * @param int $limit
     * @return array|Recipe[]
     */
    public function fetchByCuisine($cuisine, $offset = 0, $limit = null)
    {
        $cuisine_recipes = array();
        foreach ($this->recipes as $recipe) {
            if ($recipe->getRecipeCuisine() == $cuisine) {
                $cuisine_recipes[] = $recipe;
            }
        }
        return array_slice($cuisine_recipes, $offset, $limit);
    }

    /**
     * Fetches single recipe by its ID / null if not found
     *
     * @param int $id
     * @return Recipe|null
     */
    public function fetchById($id)
    {
        foreach ($this->recipes as $recipe) {
            if ($recipe->getId() == $id) {
                return $recipe;
            }
        }
        return null;
    }

    /**
     * Persistence - inserting new recipe into the CSV storage
     *
     * @param Recipe $recipe
     * @return bool success
     */
    public function insert(Recipe $recipe)
    {
        $this->recipes[] = $recipe;

        $success = $this->storage->addLine($this->serializer->normalize($recipe));

        return $success;
    }

    /**
     * Persistence - updating existing recipe in CSV storage
     *
     * @param Recipe $recipe
     * @param array $data
     * @return Recipe updated $recipe
     */
    public function updateWithData(Recipe $recipe, $data)
    {
        $recipe->setUpdatedAt(new \DateTime());
        $columns = $this->serializer->normalize($recipe);

        // update columns contained in received data:
        foreach ($data as $name => $value) {
            if (array_key_exists($name, $columns)) {
                $columns[$name] = $value;
            }
        }
        // Replace line that has this id in first column:
        $this->storage->replaceLineBasedOnColumnValue(0, $recipe->getId(), $columns);

        // refresh recipes:
        $this->loadRecipesFromCsv();

        // return updated recipe:
        return $this->serializer->denormalize(
            $columns,
            self::DATA_CLASS
        );
    }

}