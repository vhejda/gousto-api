<?php

namespace GoustoApiBundle\Entity;

use GoustoApiBundle\Component\Csv;
use Symfony\Component\Serializer\Serializer;

class RecipeRatingRepository
{
    const DATA_FILE = "data/recipe_rating.csv";
    const DATA_CLASS = 'GoustoApiBundle\Entity\RecipeRating';

    /**
     * @var Csv
     */
    protected $storage;

    /**
     * @var array|RecipeRating[]
     */
    protected $ratings;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @var string path to the application root
     */
    protected $app_root;

    /**
     * @param Serializer $serializer
     * @param string $kernel_root
     * @param Csv $storage
     */
    public function __construct($serializer, $kernel_root, Csv $storage = null)
    {
        $this->serializer = $serializer;
        $this->app_root = $kernel_root . "/../";

        // if not open, open CSV containing the recipe data:
        if ($storage === null) {
            $this->storage = new Csv($this->app_root . self::DATA_FILE);
        }else{
            $this->storage = $storage;
        }

        // Load ratings:
        $this->loadRecipeRatingsFromCsv();
    }

    /**
     * @param Csv $storage
     */
    public function setStorage(Csv $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return Csv
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * Lazy loading of the recipe ratings
     */
    private function loadRecipeRatingsFromCsv()
    {
        $this->storage->rewind();
        // Get column headers from the first line of the CSV:
        $column_names = $this->storage->getNextLine();

        // Instantiate array of RecipeRating objects by denormalizing assoc. array returned from CSV line
        // Denormalization uses "Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer"
        // with "CamelCaseToSnakeCaseNameConverter"
        $this->ratings = array();
        while ($values = $this->storage->getNextLine()) {
            $this->ratings[] = $this->serializer->denormalize(
                array_combine($column_names, $values),
                self::DATA_CLASS
            );
        }
    }

    // --------------------------------------------
    //  Repository methods:
    // --------------------------------------------

    /**
     * Creates new rating
     *
     * @param int $recipeId
     * @param int $rating
     * @return RecipeRating
     */
    public function create($recipeId, $rating)
    {
        $rating = new RecipeRating($recipeId, $rating);
        $rating->setCreatedAt(new \DateTime());
        return $rating;
    }

    /**
     * Persistence - inserting new rating into the CSV storage
     *
     * @param RecipeRating $rating
     * @return bool success
     */
    public function insert(RecipeRating $rating)
    {
        $this->ratings[] = $rating;

        $success = $this->storage->addLine($this->serializer->normalize($rating));

        return $success;
    }

    /**
     * Fetches ratings for specific recipe
     *
     * @param int $recipeId
     * @param int $offset
     * @param int $limit
     * @return array|RecipeRating[]
     */
    public function fetchByRecipe($recipeId, $offset = 0, $limit = null)
    {
        $recipe_ratings = array();
        foreach ($this->ratings as $rating) {
            if ($rating->getRecipeId() == $recipeId) {
                $recipe_ratings[] = $rating;
            }
        }
        return array_slice($recipe_ratings, $offset, $limit);
    }

}