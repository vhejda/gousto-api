<?php

namespace GoustoApiBundle\Entity;

class RecipeRating
{

    const DATETIME_FORMAT = "d/m/Y H:i:s";

    /** @var int $recipeId */
    protected $recipeId;

    /** @var int $rating 1-5 */
    protected $rating;

    /** @var \DateTime $createdAt */
    protected $createdAt;

    /**
     * RecipeRating constructor.
     * @param int $recipeId
     * @param int $rating
     */
    public function __construct($recipeId, $rating)
    {
        $this->recipeId = (int) $recipeId;
        $this->rating = (int) $rating;
    }

    /**
     * @return string Date in self::DATETIME_FORMAT format
     */
    public function getCreatedAt()
    {
        return $this->createdAt->format(self::DATETIME_FORMAT);
    }

    /**
     * @param string|\DateTime $createdAt
     * @return RecipeRating
     */
    public function setCreatedAt($createdAt)
    {
        if ($createdAt instanceof \DateTime) {
            $this->createdAt = $createdAt;
        } elseif (is_string($createdAt)){
            $this->createdAt = \DateTime::createFromFormat(self::DATETIME_FORMAT, $createdAt);
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getRecipeId()
    {
        return $this->recipeId;
    }

    /**
     * @return int 1-5
     */
    public function getRating()
    {
        return $this->rating;
    }

}