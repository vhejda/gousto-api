<?php

namespace Tests\GoustoApiBundle\Entity;

use GoustoApiBundle\Component\Csv;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Functional tests for ApiController
 * @package Tests\GoustoApiBundle\Entity
 */
class ApiControllerTest extends WebTestCase
{
    public function urlProvider()
    {
        return array(
            array('/api/recipes.json'),
            array('/api/recipes/1.json'),
            array('/api/recipes/1/ratings.json')
        );
    }

    public static $testData = array(
        "box_type" => "special",
        "title" => "Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad",
        "short_title" => "",
        "marketing_description" => "Here we've used onglet steak which is an extra flavoursome...",
        "calories_kcal" => "401",
        "protein_grams" => "12",
        "fat_grams" => "35",
        "carbs_grams" => "0",
        "bulletpoint1" => "",
        "bulletpoint2" => "",
        "bulletpoint3" => "",
        "recipe_diet_type_id" => "meat",
        "season" => "all",
        "base" => "noodles",
        "protein_source" => "beef",
        "preparation_time_minutes" => "35",
        "shelf_life_days" => "4",
        "equipment_needed" => "Appetite",
        "origin_country" => "Great Britain",
        "recipe_cuisine" => "asian",
        "in_your_box" => "",
        "gousto_reference" => "59"
    );

    /**
     * @dataProvider urlProvider
     */
    public function testGetIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function testPostRecipes()
    {
        $client = self::createClient();
        $client->request('POST', '/api/recipes.json', array(), array(), array(), json_encode(self::$testData));

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    public function testPutRecipe()
    {
        $client = self::createClient();
        $client->request('PUT', '/api/recipes/1.json', array(), array(), array(), json_encode(self::$testData));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testPostRecipeRatings()
    {
        $client = self::createClient();
        $client->request(
            'POST', '/api/recipes/1/ratings.json', array(), array(), array(), json_encode(array("rating" => 5))
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

}