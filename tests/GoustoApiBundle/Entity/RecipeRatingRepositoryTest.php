<?php

namespace Tests\GoustoApiBundle\Entity;

use GoustoApiBundle\Component\Csv;
use GoustoApiBundle\Entity\RecipeRating;
use GoustoApiBundle\Entity\RecipeRatingRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RecipeRatingRepositoryTest extends WebTestCase
{
    /** @var RecipeRatingRepository */
    public static $repository;

    public static $testCsvCopy;

    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        // Get repository service with its dependencies:
        self::$repository = $kernel->getContainer()->get('gousto.recipe_rating.repository');

        // Copy test CSV for testing purposes:
        $test_csv = $kernel->getRootDir() . '/../data/test_recipe_rating.csv';
        $test_csv_copy = $kernel->getRootDir() . '/../data/test_recipe_rating_copy.csv';
        copy($test_csv, $test_csv_copy);
        self::$testCsvCopy = $test_csv_copy;

        // Load test data storage:
        $test_storage = new Csv($test_csv_copy);
        self::$repository->setStorage($test_storage);
    }

    public function testCreate()
    {
        $now = new \DateTime();
        $rating = self::$repository->create(3, 3);

        // do I get correct instance?
        $this->assertInstanceOf('GoustoApiBundle\Entity\RecipeRating', $rating);

        $this->assertEquals(3, $rating->getRecipeId());
        $this->assertEquals(3, $rating->getRating());

        // createdAt set correctly?
        $this->assertEquals($now->format(RecipeRating::DATETIME_FORMAT), $rating->getCreatedAt());
    }

    public function testFetchByRecipe()
    {
        $ratings = self::$repository->fetchByRecipe(2);
        $this->assertCount(4, $ratings);

        $this->assertInstanceOf('GoustoApiBundle\Entity\RecipeRating', $ratings[0]);
    }

    public function testInsert()
    {
        $rating = self::$repository->create(2, 1);

        self::$repository->insert($rating);
        $ratings = self::$repository->fetchByRecipe(2);

        $this->assertCount(5, $ratings);
    }

}