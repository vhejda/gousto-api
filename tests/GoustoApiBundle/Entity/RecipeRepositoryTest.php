<?php

namespace Tests\GoustoApiBundle\Entity;

use GoustoApiBundle\Component\Csv;
use GoustoApiBundle\Entity\Recipe;
use GoustoApiBundle\Entity\RecipeRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RecipeRepositoryTest extends WebTestCase
{
    /** @var RecipeRepository */
    public static $repository;

    public static $testData = array(
        "box_type" => "special",
        "title" => "Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad",
        "short_title" => "",
        "marketing_description" => "Here we've used onglet steak which is an extra flavoursome...",
        "calories_kcal" => "401",
        "protein_grams" => "12",
        "fat_grams" => "35",
        "carbs_grams" => "0",
        "bulletpoint1" => "",
        "bulletpoint2" => "",
        "bulletpoint3" => "",
        "recipe_diet_type_id" => "meat",
        "season" => "all",
        "base" => "noodles",
        "protein_source" => "beef",
        "preparation_time_minutes" => "35",
        "shelf_life_days" => "4",
        "equipment_needed" => "Appetite",
        "origin_country" => "Great Britain",
        "recipe_cuisine" => "asian",
        "in_your_box" => "",
        "gousto_reference" => "59"
    );

    public static $testCsvCopy;

    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        // Get repository service with its dependencies:
        self::$repository = $kernel->getContainer()->get('gousto.recipe.repository');

        // Copy test CSV for testing purposes:
        $test_csv = $kernel->getRootDir() . '/../data/test_recipe.csv';
        $test_csv_copy = $kernel->getRootDir() . '/../data/test_recipe_copy.csv';
        copy($test_csv, $test_csv_copy);
        self::$testCsvCopy = $test_csv_copy;

        // Load test data storage:
        $test_storage = new Csv($test_csv_copy);
        self::$repository->setStorage($test_storage);
    }

    public function testCreateFromData()
    {
        $now = new \DateTime();
        $recipe = self::$repository->createFromData(self::$testData);

        // do I get correct instance?
        $this->assertInstanceOf('GoustoApiBundle\Entity\Recipe', $recipe);

        // one assertion for basic getters:
        $this->assertEquals("Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad", $recipe->getTitle());

        // one assertion for slug:
        $this->assertEquals("sweet-chilli-and-lime-beef-on-a-crunchy-fresh-noodle-salad", $recipe->getSlug());

        // one assertion for DateTime getters:
        $this->assertEquals($now->format(Recipe::DATETIME_FORMAT), $recipe->getCreatedAt());
    }

    public function testFetchAll()
    {
        $all_recipes = self::$repository->fetchAll();
        $this->assertCount(10, $all_recipes);
        $this->assertInstanceOf('GoustoApiBundle\Entity\Recipe', $all_recipes[0]);

        $first_2_recipes = self::$repository->fetchAll(0, 2);
        $this->assertCount(2, $first_2_recipes);
        $this->assertEquals(1, $first_2_recipes[0]->getId());
        $this->assertEquals(2, $first_2_recipes[1]->getId());

        $last_2_recipes = self::$repository->fetchAll(8);
        $this->assertCount(2, $last_2_recipes);
        $this->assertEquals(9, $last_2_recipes[0]->getId());
        $this->assertEquals(10, $last_2_recipes[1]->getId());
    }

    public function testFetchByCuisine()
    {
        $british_recipes = self::$repository->fetchByCuisine("british");
        $this->assertCount(4, $british_recipes);
        $this->assertInstanceOf('GoustoApiBundle\Entity\Recipe', $british_recipes[0]);
    }

    public function testFetchById()
    {
        $recipe = self::$repository->fetchById(6);
        $this->assertEquals(6, $recipe->getId());
        $this->assertInstanceOf('GoustoApiBundle\Entity\Recipe', $recipe);
    }

    public function testInsert()
    {
        $recipe = self::$repository->createFromData(self::$testData);

        self::$repository->insert($recipe);
        $recipes = self::$repository->fetchAll();

        $this->assertCount(11, $recipes);
    }

    public function testUpdateWithData()
    {
        $recipe = self::$repository->fetchById(6);
        $old_cal = $recipe->getCaloriesKcal();

        self::$repository->updateWithData($recipe, array("calories_kcal" => ($old_cal + 1)));

        $recipe = self::$repository->fetchById(6);
        $new_cal = $recipe->getCaloriesKcal();

        $this->assertNotEquals($old_cal, $new_cal);

    }

}